<?php
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: text/html; charset=utf-8");

    require "viatges_bdd.php";
    require "configuracio_bdd.php";
    
    $bdd = new ViatgesBDD($db_host, $db_user, $db_pass, $db_name);

    echo json_encode($bdd->getTotesExperiencies());
?>
