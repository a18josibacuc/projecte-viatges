SELECT * FROM a18josibacuc_ProjecteViatges.usuaris;

SELECT * FROM a18josibacuc_ProjecteViatges.experiencies;

INSERT INTO `a18josibacuc_ProjecteViatges`.`categories` (nom) VALUES
	('aquàtic');

INSERT INTO `a18josibacuc_ProjecteViatges`.`experiencies` (titol, data, text, imatge, coordenades, num_valoracions, estat, codi_categoria, codi_usuari) VALUES
	('Cayak per la cova d\'en Gispert','2019-12-09','Entre les roques de Tamariu es dóna un fenomen extraordinari i espectacular: la combinació de la forma de la Cova d\'en Gispert amb l\'orientació cap a l\'Est fa que, dues vegades l\'any, el Sol surti perfectament alineat amb la cova. Això fa que es pugui contemplar la immensitat de la cova i capturar un instant de postal com n\'hi ha pocs. Estigueu atents, és una excursió reservada a uns pocs privilegiats i que es farà durant la segona quinzena d\'agost. És recomanable haver pujat a algun caiac en travessia abans.','https://media.timeout.com/images/103432605/380/285/image.jpg','E1°52\'3.65"','1','esborrany','1','1');
;
    
INSERT INTO `a18josibacuc_ProjecteViatges`.`usuaris` (alias, password, data_registre, reputacio) VALUES
	('Cimirro', '1234', '2019-12-09', 'iniciat')
    ;
    
SELECT * FROM usuaris;
SELECT codi FROM usuaris WHERE alias = 'JoseC92' and password = '1234';
    
DELETE FROM usuaris WHERE alias='JoseC92';
