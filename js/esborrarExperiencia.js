function esborrarExperiencia(codi) {
    let params = new URLSearchParams();
    params.append('codiExp', codi);

    axios.post('esborrarExperiencia.php', params).then(function(response) {
        if(response.data == "¡Operación realizada correctamente!") {
            $.amaran({
                content:{
                    title:'Esborrada!',
                    message:`Codi de l'experiència: ${codi}`,
                    info:'',
                    icon:'fa fa-check'
                },
                theme:'awesome ok'
            });

          } else {
            $.amaran({
                content:{
                    title:'No s\'ha pogut esborrar!',
                    message:'',
                    info:'',
                    icon:'fa fa-error'
                },
                theme:'awesome error'
            });
          }
      });
}
