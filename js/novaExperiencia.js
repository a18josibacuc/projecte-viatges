document.getElementById("crear_exp").addEventListener('click', function() {
    novaExperiencia();
});

function novaExperiencia() {
    let titolExp = document.getElementsByName("titol_exp")[0].value;
    let descripcioExp = CKEDITOR.instances.descripcio_exp.getData();
    let imatgeExp = document.getElementsByName("imatge_exp")[0].value;
    let coordenadesExp = document.getElementsByName("coordenades_exp")[0].value;
    let categoriaExp = document.getElementsByName("categoria_exp")[0].value;
    let estatExp = document.getElementsByName("estat_exp")[0].value;

    let params = new URLSearchParams();
    params.append('titolExp', titolExp);
    params.append('descripcioExp', descripcioExp);
    params.append('categoriaExp', categoriaExp);
    params.append('estatExp', estatExp);
    params.append('coordenadesExp', coordenadesExp);
    params.append('imatgeExp', imatgeExp);

    axios.post('novaExperiencia.php', params).then(function(response) {
        console.log(response.data)
        if(response.data == "¡Operación realizada correctamente!") {
            Swal.fire({
                imageUrl: 'img/bojak.png',
                title: 'Experiència afegida!'
            })

          } else {
            Swal.fire({
                icon: 'error',
                title: 'No s\'ha pogut afegir!'
            })
          }
    });


}



