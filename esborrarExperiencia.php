<?php
    header("Content-Type: text/html;charset=utf-8");
    header("Access-Control-Allow-Origin: *");

    session_start();

    require "viatges_bdd.php";
    require "configuracio_bdd.php";

    if (!isset($_POST['codiExp'])) {
        die ('Completa tots els camps!');
     }

    $bdd = new ViatgesBDD($db_host, $db_user, $db_pass, $db_name);
    $bdd->deleteExperienciaByCodiAndAlias($_POST['codiExp'], $_SESSION['user_id']);
?>
