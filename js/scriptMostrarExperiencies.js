filtreUsuari.addEventListener('click', function(){
  let node=document.getElementById('targetesExp');
  while (node.hasChildNodes()) {  
    node.removeChild(node.firstChild);
  }
  axios({
    method: 'post',
    url: 'experienciesUsuari.php',
    responseType: 'json'
  }).then(function (response) {
    console.log(response.data);
    let experiencies= document.getElementById("targetesExp");
    let num_experiencies= response.data.length;
    response.data.reverse();
    for (let index = 0; index < num_experiencies; index++) {
        experiencies.innerHTML +=
        `
        <div class="col-md-4">
          <div class="card mb-4 shadow-sm">
          <img src="${response.data[index].imatge}" class="bd-placeholder-img card-img-top" width="100%" height="225"</img> <button data-toggle="modal" data-target="#modalEditarImatge${response.data[index].codi}"  type="button" class="btn btn-outline-dark"><i class="fas fa-edit"> </i> Editar</button>

          <div class="card-body">
              <p class="card-text">
                <h3>${response.data[index].titol}</h3><button data-toggle="modal" data-target="#modalEditarTitol${response.data[index].codi}"  type="button" class="btn btn-sm btn-outline-dark"><i class="fas fa-edit"> </i> Editar</button>
              </p>
              <p>Publicat per: ${response.data[index].alias_usuari}</p>

              <div id="textModal${response.data[index].codi}" class="modal fade" role="dialog">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                    <h4 class="modal-title">Descripció</h4>
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body">
                      <button data-toggle="modal" data-target="#modalEditarText${response.data[index].codi}"  type="button" class="btn btn-sm btn-outline-dark">  <i class="fas fa-edit"> </i>  Editar</button>
                      ${response.data[index].text}
                    </div>
                  </div>
                </div>
              </div>

              <small class="text-muted">Data de publicació: ${response.data[index].data}</small>
              <div class="d-flex justify-content-between align-items-center">
              <div class="btn-group btn-group-sm mt-2 float-right" role="group"">
                <button type="button" class="btn btn-outline-dark btn-sm" data-toggle="modal" data-target="#textModal${response.data[index].codi}">Descripció</button>
                <button onclick="esborrarExperiencia(${response.data[index].codi})" type="button" class="btn btn-outline-danger"><i class="fas fa-trash"> </i> Esborrar</button>
              </div>
              </div>
          </div>
          </div>
        </div>

        <div class="modal fade" id="modalEditarImatge${response.data[index].codi}">
        <div class="modal-dialog">
            <div class="modal-content">
    
              <div class="modal-header">
                <h4 class="modal-title">Editar imatge</h4>
                <button type="button" class="btn btn-outline-dark btn-lg" data-dismiss="modal">&times;</button>
              </div>
    
              <div class="modal-body">
                <div class="row">
                  <div class="col-md-6 mb-3">
                      <label for="titol">Nova imatge</label>
                      <input type="text" maxlength="128" class="form-control" name="imatge_exp_editar${response.data[index].codi}">
                        <div class="invalid-feedback">
                            Requereix una imatge de expèriencia.
                        </div>
                  </div>
                </div>
              </div>
    
              <div class="modal-footer mt-2">
                <button onclick="editaExperiencia(${response.data[index].codi})" type="button" class="btn btn-success btn-block" data-dismiss="modal"><i class="fas fa-plus-circle"></i> D E S A R</button>
              </div>
            </div>
          </div>
          </div>
        </div>
    
        <div class="modal fade" id="modalEditarTitol${response.data[index].codi}">
        <div class="modal-dialog">
            <div class="modal-content">
    
              <div class="modal-header">
                <h4 class="modal-title">Editar títol</h4>
                <button type="button" class="btn btn-outline-dark btn-lg" data-dismiss="modal">&times;</button>
              </div>
    
              <div class="modal-body">
                <div class="row">
                  <div class="col-md-6 mb-3">
                      <label for="titol">Nou títol</label>
                      <input type="text" maxlength="128" class="form-control" name="titol_exp_editar${response.data[index].codi}">
                        <div class="invalid-feedback">
                            Requereix un títol de expèriencia.
                        </div>
                  </div>
                </div>
              </div>
    
              <div class="modal-footer mt-2">
                <button onclick="editaExperiencia(${response.data[index].codi})" type="button" class="btn btn-success btn-block" data-dismiss="modal"><i class="fas fa-plus-circle"></i> D E S A R</button>
              </div>
            </div>
          </div>
          </div>
        </div>
    
        <div class="modal fade" id="modalEditarText${response.data[index].codi}">
        <div class="modal-dialog">
            <div class="modal-content">
    
              <div class="modal-header">
                <h4 class="modal-title">Editar descripció</h4>
                <button type="button" class="btn btn-outline-dark btn-lg" data-dismiss="modal">&times;</button>
              </div>
    
              <div class="modal-body">
                <div class="row">
                  <div class="col-md-6 mb-3">
                      <label for="titol">Nova descripció</label>
                      <textarea name="descripcio_exp_editar${response.data[index].codi}" rows="10" cols="50">${response.data[index].text}</textarea>
                        <div class="invalid-feedback">
                            Requereix una descripció de expèriencia.
                        </div>
                  </div>
                </div>
              </div>
    
              <div class="modal-footer mt-2">
                <button onclick="editaExperiencia(${response.data[index].codi})" type="button" class="btn btn-success btn-block" data-dismiss="modal"><i class="fas fa-plus-circle"></i> D E S A R</button>
              </div>
            </div>
          </div>
          </div>
        </div>
        `
    }
});
})

totesExperiencies.addEventListener('click', function(){
  let node=document.getElementById('targetesExp');
  while (node.hasChildNodes()) {  
    node.removeChild(node.firstChild);
  }
  axios({
    method: 'post',
    url: 'totesExperiencies.php',
    responseType: 'json'
  }).then(function (response) {
    console.log(response.data);
    let experiencies= document.getElementById("targetesExp");
    let num_experiencies= response.data.length;
    response.data.reverse();
    for (let index = 0; index < num_experiencies; index++) {
        experiencies.innerHTML +=
        `
        <div class="col-md-4">
          <div class="card mb-4 shadow-sm">
          <img src="${response.data[index].imatge}" class="bd-placeholder-img card-img-top" width="100%" height="225"</img>
          <div class="card-body">
              <p class="card-text">
                <h3>${response.data[index].titol}</h3>
              </p>
              <p>Publicat per: ${response.data[index].alias_usuari}</p>
              
              <div id="textModal${response.data[index].codi}" class="modal fade" role="dialog">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                    <h4 class="modal-title">Descripció</h4>
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body">
                      ${response.data[index].text}
                    </div>
                  </div>
                </div>
              </div>
              
              <small class="text-muted">Data de publicació: ${response.data[index].data}</small>
              <div class="d-flex justify-content-between align-items-center">
              <div class="btn-group btn-group-sm mt-2 float-right" role="group"">
                <button type="button" class="btn btn-outline-dark btn-sm" data-toggle="modal" data-target="#textModal${response.data[index].codi}">Descripció</button>
                <button id="Pos${response.data[index].codi}" onclick="punctuarPosExp(${response.data[index].codi})" type="button" class=" btn btn-outline-success">${response.data[index].num_valoracions_pos}</button>
                <button id="Neg${response.data[index].codi}" onclick="punctuarNegExp(${response.data[index].codi})" type="button" class=" btn btn-outline-danger">${response.data[index].num_valoracions_neg}</button>
              </div>
              </div>
          </div>
          </div>
        </div>
        `
    }
  });
})

function selCategoria(){
  let categoria = document.getElementById('selCategoria').value;
  let params = new URLSearchParams();
  params.append('categoria', categoria);

  axios.post('experienciesCategoria.php', params).then(function (response) {
    if(response.data.funciona!="no" && Object.keys(response.data[0]).length != 1){
      let node=document.getElementById('targetesExp');
      while (node.hasChildNodes()) {  
        node.removeChild(node.firstChild);
      }
      let experiencies= document.getElementById("targetesExp");
      let num_experiencies= response.data.length;
      response.data.reverse();
      for (let index = 0; index < num_experiencies; index++) {
          experiencies.innerHTML +=
          `
        <div class="col-md-4">
          <div class="card mb-4 shadow-sm">
          <img src="${response.data[index].imatge}" class="bd-placeholder-img card-img-top" width="100%" height="225"</img>
          <div class="card-body">
              <p class="card-text">
                <h3>${response.data[index].titol}</h3>
              </p>
              <p>Publicat per: ${response.data[index].alias_usuari}</p>
              
              <div id="textModal${response.data[index].codi}" class="modal fade" role="dialog">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                    <h4 class="modal-title">Descripció</h4>
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                      ${response.data[index].text}
                    </div>
                  </div>
                </div>
              </div>
              
              <small class="text-muted">Data de publicació: ${response.data[index].data}</small>
              <div class="d-flex justify-content-between align-items-center">
              <div class="btn-group btn-group-sm mt-2 float-right" role="group"">
                <button type="button" class="btn btn-outline-dark btn-sm" data-toggle="modal" data-target="#textModal${response.data[index].codi}">Descripció</button>
                <button id="Pos${response.data[index].codi}" onclick="punctuarPosExp(${response.data[index].codi})" type="button" class=" btn btn-outline-success">${response.data[index].num_valoracions_pos}</button>
                <button id="Neg${response.data[index].codi}" onclick="punctuarNegExp(${response.data[index].codi})" type="button" class=" btn btn-outline-danger">${response.data[index].num_valoracions_neg}</button>
              </div>
              </div>
          </div>
          </div>
        </div>
        `
      }
    } else {
      let experiencies= document.getElementById("targetesExp");
      experiencies.innerHTML = "<h3 id='empty'>No n'hi ha cap encara!</h3>";
    }
  })
}
