<!DOCTYPE html>
<html lang="cat">
<head>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <title>Viatges Caribdis</title>
    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
    <link rel="icon" href="img/favicon.ico" type="image/x-icon">

    <!-- Esto es BOOTSTRAP-->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.15.1/jquery.validate.min.js"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="css/amaran.min.css">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src="https://cdn.ckeditor.com/4.13.1/standard/ckeditor.js"></script>

    <!---------------->
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <script type="text/javascript" src="js/carregarVideo.js"></script>

</head>
<body>
    <header>
        <div class="navbar navbar-dark bg-dark">
            <div class="container d-flex justify-content-between">
              <a href="index.php" class="navbar-brand d-flex align-items-center">
                  <img src="img/logo1.png" width="850px">
              </a>

                <div class="btn-group">
                  <button type="button" class="btn btn-outline-light btn-lg" data-toggle="modal" data-target="#loginModal"><i class="fas fa-user"></i> Login</button>
                  <button type="button" class="btn btn-outline-light btn-lg" data-toggle="modal" data-target="#registreModal"><i class="fas fa-key"></i> Registre</button>

                </div>

                <div id="loginModal" class="modal fade">
                  <div class="modal-dialog modal-login">
                    <div class="modal-content">
                      
                        <div class="modal-header">				
                          <h2 class="modal-title">Login</h2>
                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body">				
                          <div class="form-group">
                            <label>Usuari</label>
                            <input type="text" class="form-control usuario" required="required">
                          </div>
                          <div class="form-group">
                            <div class="clearfix">
                              <label>Clau</label>
                            </div>
                            
                            <input type="password" class="form-control password" required="required">
                          </div>
                        </div>
                        <div class="modal-footer">
                          <button data-dismiss="modal" type="submit" class="btn btn-outline-dark pull-right enviar">Login</button>
                        </div>
                    </div>
                  </div>
                </div>  

                <div id="registreModal" class="modal fade">
                  <div class="modal-dialog modal-login">
                    <div class="modal-content">
                        <div class="modal-header">				
                          <h2 class="modal-title">Registre</h2>
                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body">				
                          <div class="form-group">
                            <label>Usuari</label>
                            <input type="text" class="form-control usuario" required="required">
                          </div>
                          <div class="form-group">
                            <div class="clearfix">
                              <label>Clau</label>
                            </div>
                            
                            <input type="password" class="form-control password" required="required">
                          </div>
                        </div>
                        <div class="modal-footer">
                          <button data-dismiss="modal" type="submit" class="btn btn-outline-dark pull-right enviar">Registrar</button>
                        </div>
                    </div>
                  </div>
                </div> 
            </div>
        </div>
    </header>

<main role="main">
    <div class="jumbotron jumbotron-fluid">

    <video autoplay muted loop>    
        <source src="" data-src="https://imgur.com/f5mEAvl.mp4" type="video/mp4">
    </video>

      <div class="container text-white">
        <h1 class="display-10">Gaudeix d'unes bones vacances!</h1>
      </div>
    </div>


    <div id="pag_personal">
        <h3 class="col-md-12 bg-dark subtitol mb-5">El meu espai</h3>

        <div class="row m-2 p-5">
          <div class="card col-2" style="width: 18rem;">
            <img class="card-img-top img-fluid" src="img/travel_icon.png" alt="Card image cap">
            <div id="userInfo" class="card-body">
              <h5 id="usuari_personal" class="card-title"></h5>
              <button id="adm"></button>
              <button type="button" class="btn btn-outline-dark btn-lg btn-block" data-toggle="modal" data-target="#editarModal">
                <i class="fas fa-user-edit"></i> Editar informació personal
              </button>
              
              <button type="button" class="btn btn-outline-dark btn-lg mt-1 btn-block" data-toggle="modal" data-target="#crearExpModal"><i class="fas fa-plus"></i> Afegir nova experiència</button>
              <button type="button" class="btn btn-danger btn-lg mt-1 btn-block" data-toggle="modal" data-target="#sortirModal" data-dismiss="modal"><i class="fas fa-sign-out-alt"></i> Sortir</button>
              
              <div class="modal fade" id="editarModal">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h4 class="modal-title">Editar informació</h4>
                      <button type="button" class="btn btn-outline-dark btn-lg" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                    Nou alias
                    <br>
                    <input type="text" name="nou_alias">
                    <br>
                    Nou password
                    <br>
                    <input type="text" name="nou_password">
                    <br><br>
                    <button id="enviar_editar" type="button" class="btn btn-outline-dark btn-lg" data-dismiss="modal">Enviar</button>
                    </div>
                  </div>
                </div>
              </div>
              <div class="modal fade" id="adminModal">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h4 class="modal-title">Panel Administrador</h4>
                      <button type="button" class="btn btn-outline-dark btn-lg" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                      <h6>Afegir Categoria</h6>
                      <input type="text" name="novaCat">
                      <button id="btnCat">Afegir</button>
                      <h6>Eliminar usuari</h6>
                      <input type="text" name="userDel">
                      <button id="btnUser">Eliminar</button>
                    </div>
                  </div>
                </div>
              </div>
              <div class="modal fade" id="sortirModal">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h4 class="modal-title">Vols tancar la sessió?</h4>
                    </div>
                    <div class="modal-body">
                      <button id="tancar_sessio" type="button" class="btn btn-danger btn-block" data-dismiss="modal">Tancar sessió</button>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-outline-dark btn-lg" data-dismiss="modal">Tancar</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="col-2">
            <div class="list-group" id="list-tab" role="tablist">
              <a class="list-group-item list-group-item-action active" id="totesExperiencies" data-toggle="list" href="#list-home" role="tab" aria-controls="home">Totes les experiències</a>
              <a class="list-group-item list-group-item-action" id="filtreUsuari" data-toggle="list" href="#list-home" role="tab" aria-controls="home">Les meves experiències</a>
              <a class="list-group-item"><h5><strong>Filtrar per categoria</strong></h5></a>
              <select id="selCategoria" class="custom-select d-block w-100" onchange="selCategoria()">
                    <option value="Opcions...">Opcions...</option>
                    <option value="Aventures">Aventures</option>
                    <option value="Muntanyisme">Muntanyisme</option>
                    <option value="Historic">Històric</option>
                    <option value="Familiar">Familiar</option>
                    <option value="Romantic">Romàntic</option>
                </select>
            </div>
          </div>
          <div id="targetesExp" class="col-8 row">
            <script src="js/mostrarExperiencies.js"></script>
          </div>
        </div>

    </div>

    <div class="modal fade" id="crearExpModal">
      <div class="modal-dialog">
        <div class="modal-content">

          <div class="modal-header">
            <h4 class="modal-title">Nova experiència</h4>
            <button type="button" class="btn btn-outline-dark btn-lg" data-dismiss="modal">&times;</button>
          </div>

          <div class="modal-body">
          <div class="row">
            <div class="col-md-6 mb-3">
                <label for="titol">Titol de l'experiència</label>
                <input type="text" maxlength="128" class="form-control" name="titol_exp">
                <div class="invalid-feedback">
                    Requereix un títol de expèriencia.
                </div>
            </div>
        </div>
        <div class="mb-3">
            <label for="descripcio">Descripció de la expèriencia</label>
            <div class="input-group">
                <textarea id="descripcio_exp" name="descripcio_exp" maxlength="1500" rows="5" cols="400"></textarea>
                <script>
                        CKEDITOR.replace( 'descripcio_exp' );
                </script>
                <div class="invalid-feedback" style="width: 100%;">
                    La descripció de la expèriencia és necesaria.
                </div>
            </div>
        </div>
        <div class="mb-3">
            <label for="foto">Foto de l'experiència </label>
            <br>
            <input type="text" name="imatge_exp">
            <div class="upload"></div>
            <div class="invalid-feedback">
                Es necesari adjuntar una imatge de la experiència per a la publicació.
            </div>
        </div>
        <div class="mb-3">
            <label for="coordenades">Lloc de la expèriencia (Coordenades Google Maps)</label>
            <input type="text" class="form-control" name="coordenades_exp">
            <div class="invalid-feedback">
                Necesari posar les coordenades adjuntes del Google Maps.
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <label for="categoria">Escull una categoria</label>
                <select class="custom-select d-block w-100" name="categoria_exp">
                    <option value="">Opcions...</option>
                    <option value="1">Aventures</option>
                    <option value="2">Muntanyisme</option>
                    <option value="3">Històric</option>
                    <option value="4">Familiar</option>
                    <option value="5">Romàntic</option>
                </select>
                <div class="invalid-feedback">
                    Necesari escollir un tipus de categoria.
                </div>
            </div>
            <div class="col-md-6">
                <label for="estat">Estat de la expèriencia</label>
                <select name="estat_exp" class="custom-select d-block w-100" id="estat" required="">
                  <option value="">Opcions...</option>
                  <option>Esborrany</option>
                  <option>Publicada</option>
                  <option>Rebutjada</option>
                </select>
                <div class="invalid-feedback">
                    Introdueix l'estat de la experiencia.
                </div>
            </div>
          </div>

          <div class="modal-footer mt-2">
            <button id="crear_exp" type="button" class="btn btn-success btn-block" data-dismiss="modal"><i class="fas fa-plus-circle"></i>  C R E A R</button>
          </div>

        </div>
      </div>
      </div>
    </div>

    <div id="darreres_exp">
      <h3 class="col-md-12 bg-dark subtitol mt-0">Darreres experiències</h3>
      <div class="album py-5 bg-light">
        <div class="container">
          <div id="experiencies" class="row">
          
          </div>
        </div>
      </div>
    </div>
</main>

  <footer class="text-muted bg-dark">
    <div class="col-md-12" id="footer">
      <img src="img/logo2.png" width="125px">
      <p class="col-md-2">© 2019 Viatges Caribdis</p>
      <p class="col-md-2">
        <a href="https://twitter.com/login?lang=ca"><i class="fab fa-twitter"></i></a>
        <a href="https://ca-es.facebook.com/"><i class="fab fa-facebook"></i></a>
        <a href="https://www.instagram.com/"><i class="fab fa-instagram"></i></a>
        <a href="https://www.google.com/"><i class="fab fa-google-plus-g"></i></a>
      </p>
      <p class="col-md-2" id="back">
          <a href="#">Tornar cap amunt</a>
    </p>
    </div>
  </footer>

  <script src="js/login.js"></script>
  <script src="js/registre.js"></script>
  <script src="js/darreresExperiencies.js"></script>
  <script src="js/editarDades.js"></script>
  <script src="js/missatgeBenvinguda.js"></script>
  <script src="js/logout.js"></script>
  <script src="js/novaExperiencia.js"></script>
  <script src="js/scriptMostrarExperiencies.js"></script>
  <script src="js/punctuarExp.js"></script>
  <script src="js/editarExperiencia.js"></script>
  <script src="js/esborrarExperiencia.js"></script>
  <script src="js/afegirCategoria.js"></script>
  <script src="js/borrarUsuari.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/cookieconsent@3/build/cookieconsent.min.js" data-cfasync="false"></script>

  <script src="js/jquery.amaran.min.js"></script>
</body>

</html>
