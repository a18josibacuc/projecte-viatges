<?php
   header("Content-Type: text/html;charset=utf-8");
   header("Access-Control-Allow-Origin: *");

    session_start();

   require "viatges_bdd.php";
   require "configuracio_bdd.php";

   if (!isset($_POST['titolExp'], $_POST['descripcioExp'], $_POST['categoriaExp'], $_POST['estatExp'], $_POST['coordenadesExp'], $_POST['imatgeExp'], $_SESSION['user_id'])) {
      die ('Completa tots els camps!');
   }

   $bdd = new ViatgesBDD($db_host, $db_user, $db_pass, $db_name);
   $bdd->insertExperiencia($_POST['titolExp'], $_POST['descripcioExp'], $_POST["imatgeExp"], $_POST['coordenadesExp'], $_POST['estatExp'], $_POST['categoriaExp'], $_SESSION['user_id']);
?>
