<?php
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: text/html; charset=utf-8");

    require "viatges_bdd.php";
    require "configuracio_bdd.php";

    if($_POST['novaCategoria']==""){
        die('Escriu una categoria, si us plau.');
    }

    $bdd = new ViatgesBDD($db_host, $db_user, $db_pass, $db_name);

    $bdd->insertCategoria($_POST['novaCategoria']);
?>