<?php
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: text/html; charset=utf-8");

    require "viatges_bdd.php";
    require "configuracio_bdd.php";


    if (!isset($_POST['userLog'], $_POST['passwordLog'])) {
      // Could not get the data that should have been sent.
      die ('Escriu el teu alias i la teva clau, si us plau.');
    }
    $bdd = new ViatgesBDD($db_host, $db_user, $db_pass, $db_name);
    $estat=$bdd->getUsuariByAliasAndPassword($_POST['userLog'], $_POST['passwordLog']);

    if($estat=='Ok'){
      $comprova = $bdd->getTipusUsuari($_POST['userLog']);
      $comprova=$comprova[0];
      $comprova=$comprova['reputacio'];
      $resposta=array('tipus'=>$comprova, 'estat'=>$estat);
      echo json_encode($resposta);
    }
    else{
      $resposta=array('estat'=>$estat);
      echo json_encode($resposta);
    }
?>
