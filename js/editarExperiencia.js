function editaExperiencia(codi) {
    let titolExp = document.getElementsByName(`titol_exp_editar${codi}`)[0].value;
    let descripcioExp = document.getElementsByName(`descripcio_exp_editar${codi}`)[0].value;
    let imatgeExp = document.getElementsByName(`imatge_exp_editar${codi}`)[0].value;

    let params = new URLSearchParams();
    if(titolExp.length > 0) params.append('titolExp', titolExp);
    else if(descripcioExp.length > 0) params.append('descripcioExp', descripcioExp);
    if(imatgeExp.length > 0) params.append('imatgeExp', imatgeExp);
    params.append('codiExp', codi);

    axios.post('editarExperiencia.php', params).then(function(response){
        if(response.data == "¡Operación realizada correctamente!") {
            $.amaran({
                content:{
                    title:'Edició correcte!',
                    message:'',
                    info:'',
                    icon:'fa fa-check'
                },
                theme:'awesome ok'
            });
        } else {
            $.amaran({
                content:{
                    title:'Hi ha hagut algun error!',
                    message:'',
                    info:'',
                    icon:'fa fa-error'
                },
                theme:'awesome error'
            });
          }
    })

}
